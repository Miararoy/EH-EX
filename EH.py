from time import time
# import missingno as msno
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
from mse import MSEDB

# CONSTANTS
NULL_COUNT_THRESHOLD = 0.1
VERBOSE = False
NUM_OF_EXPERIMENT = 100
LASSO_ALPHA = 0.3

# GET DATA
df = pd.read_csv("data/Data.csv")
# SET INDEX TO BE CANDIDATE ID
df = df.set_index("candidateID")

# CHECK DATA NULLABILITY FOR ESTIMATION
# msno.matrix(df)
# msno.dendrogram(df)
# msno.heatmap(df)

# SET COLUMN LISTS ACCORDING TO PROPERTY
all_columns = df.columns
all_columns_categorical = ["prop_zone_manager", "gender"]
all_columns_numerical = [
    col for col in all_columns if col not in all_columns_categorical
]
all_columns_kpi = [
    col for col in all_columns if "kpi" in col
]
all_columns_factor = [
    col for col in all_columns if "factor" in col
]

# FILL CATEGORICAL WITH MOST COMMON VALUE
for col in all_columns_categorical:
    if df[col].isna().any():
        if ((df[col].count() / df[col].isna().count()) > NULL_COUNT_THRESHOLD):
            df[[col]] = df[[col]].fillna(value=df[col].value_counts().argmax())
df = pd.get_dummies(df)

# FILL NUMERICAL WITH mean() VALUE
for col in all_columns_numerical:
    if df[col].isna().any():
        if ((df[col].count() / df[col].isna().count()) > NULL_COUNT_THRESHOLD):
            df[[col]] = df[[col]].fillna(value=df[col].mean())
        else:
            df = df.drop(col, axis=1)


# NORMALIZE ALL KPIS
df[all_columns_kpi] = pd.DataFrame(
    preprocessing.MinMaxScaler().fit_transform(df[all_columns_kpi]),
    columns=all_columns_kpi,
    index=df[all_columns_kpi].index
)

# COMBINE ALL KPIs TO ONE
df["KPI"] = df[all_columns_kpi].apply(lambda row: row.mean(), axis=1)
all_columns_kpi.append("KPI")

# PREPARE a TEST AND TRAIN DATASETS
kpi_columns = {}
for col in all_columns_kpi:
    kpi_columns[col] = df[[col]]
    df.drop(col, axis=1)

# ============ MAIN TEST LOOP ============
linear_models_per_test_size = {}
mse_per_test_size = {}
rng = range(10, 95, 5)
msedb = MSEDB(rng)

for i in range(NUM_OF_EXPERIMENT):
    for test_size_percent in rng:
        # CREATE A LINEAR MODEL PER KPI
        linear_models = {}
        for kpi in all_columns_kpi:
            x, x_test, y, y_test = train_test_split(
                df,
                kpi_columns[kpi],
                test_size=test_size_percent/100,
                random_state=int(time())
            )
            # model = linear_model.LinearRegression()
            model = linear_model.Lasso(
                alpha=LASSO_ALPHA,
                random_state=int(time())
            )
            model.fit(x, y)
            predictions = model.predict(x_test)
            mse = mean_squared_error(y_test, predictions)
            r2 = r2_score(y_test, predictions)
            linear_models[kpi] = {
                "model": model,
                "mse": mse,
                "r2": r2
            }

        linear_models_per_test_size[
            "{}%".format(test_size_percent)
        ] = linear_models

        # VALIDATE PREDICTION OF ALL kpis to MEAN
        predictions = {}
        for kpi in all_columns_kpi:
            if kpi is not "KPI":
                predictions[kpi] = list((linear_models[kpi]["model"].predict(df)))


        # print(predictions)

        predicted_df = pd.DataFrame(
            predictions,
            index=df.index
        )
        predicted_df["KPI"] = predicted_df[predicted_df.columns].apply(
            lambda row: row.mean(),
            axis=1
        )

        # COMPARE THE TWO KPI, PREDICTED VS REAL

        kpi_MSE = mean_squared_error(kpi_columns["KPI"], predicted_df["KPI"])
        msedb.update(test_size_percent, kpi_MSE)
        # mse_per_test_size[test_size_percent] = kpi_MSE
        if VERBOSE:
            for p, r in zip(
                predicted_df[["KPI"]].values, 
                kpi_columns["KPI"].values
            ):
                print("predicted = {p} --- {r} = real".format(
                    p=p.tolist().pop().tolist().pop(),
                    r=r.tolist().pop()
                ))
            print("MSE for mean(predicted_kpi) " +
                  "vs. mean(real_kpi) == {}".format(
                    kpi_MSE
                )
            )

mse_score = msedb.get()

plt.bar(
    range(len(mse_score)),
    list(mse_score.values()), 
    align='center',
    log=True
)
plt.xticks(
    range(len(mse_score)),
    list(mse_score.keys())
)
plt.title("LOG(avg(MSE)) as a function of test size")
plt.xlabel("Test Size [%]")
plt.ylabel("log(MSE)")
plt.show()