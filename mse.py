class Experiment(object):
    def __init__(self):
        self.expr = {
            "score": 0,
            "count": 0,
        }

    def update(self, score):
        new_count = self.expr["count"] + 1
        new_score = (self.expr["score"] * self.expr["count"] + score)/new_count
        self.expr["score"] = new_score
        self.expr["count"] = new_count
    
    def get_score(self):
        return self.expr["score"]


class MSEDB(object):
    def __init__(self, rng):
        self.db = {}
        for i in rng:
            self.db[i] = Experiment()

    def update(self, key, mse):
        self.db[key].update(mse)
    
    def get(self):
        return {k: self.db[k].get_score() for k in self.db}