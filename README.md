# EH data prediction

## methodology

Observing the EH dataset one can spot two key properties:

1. Missing data
2. Small dataset (only 130 rows)

thus when building the model we acted accordingly to each property

### 1. Missing data

observing the dataset with missingno library
![Missing Data](https://gitlab.com/Miararoy/EH-EX/raw/master/plots/missingDataBefore.png)

we can see that we have several missing data points, which we can divide into 5 groups

* missing data in numerical columns (factor_ columns) > 10% of column's data
* missing data in numerical columns (factor_ columns) < 10% of column's data
* missing data in categorical columns (gender) < 10% of column's data
* missing data in KPI columns (kpi_ columns) > 10% of column's data
* missing data in KPI columns (kpi_ columns) < 10% of column's data

we chose the following strategy in data preparation:

1. if missing data is numerical and small - fill missing with mean() value
2. if missing data is numerical and large - drop column
3. if missing data is categorical and small - fill missing data with argmax() value
4. normalize all numeric factor columns
5. create dummy variables for gender and manager columns
6. normalize kpis

### 2. Small dataset

our data set contains only 130 rows, therefore we need to consider the following subjects:

* Model selection - which model will provide the most reliable results (the next 10 samples or so)
* Test / Train dataset sizes. - what ratio to hold.
* Implementation method - given several kpis we wish to predict

To do that, we chose to use the following schema:

1. "Clean" the data as described above
2. Create a "mean" column which is the mean() of all other kpi columns
2. Split into train and test with a ratio ranging from 10% to 90%
3. Randomize the split with changing random seed
4. create a linear model - PER KPI. here, a linear model was chosen being less sensitive to small datasets (and simple) with lasso regulation (large number of features)
5. predict on the entire dataset (per kpi)
5. combine the mean() of the predictions of all kpi and compare to (2) using MSE metric
6. repeat this experiment N times (here 100 was taken only by time considerations)
7. create a distribution of avg MSE per test size
8. find the optimal ratio (see graph below)
9. get the model with the optimal ratio. (saved in a dictionary of ratio --> per\_kpi\_model)

Considering the optimal ratio. after running 100 experiment the distribution looks like this:
![optimal ratio](https://gitlab.com/Miararoy/EH-EX/raw/master/plots/avgMse.png)
![optimal ratio LOG](https://gitlab.com/Miararoy/EH-EX/raw/master/plots/logAvgMse.png)

we can see that that 45-50% is the optimal ratio.
these numbers seem very high, indicating a high correlated model. one can replace VEBOSE=true, and see the prediction results for each model

## 3. Time

All and all the effort taken was approx. 4 hrs of coding + 1 hr of documentation