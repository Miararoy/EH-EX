def is_rather_empty(df, col, threshold):
    return (df[col].count() / df[col].isna().count()) > threshold